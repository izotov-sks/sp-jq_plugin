/**
 * Created by ant on 11.11.15.
 */
(function($) {
    $.fn.markMe = function(options) {

        var $self = this;

        var defaultSettings = {
            withForm: false,
            gskey: "1wcBTKMX_CazSh1qUk0lcFv-UvriMEnr-fE7Kau0k2Sg"
        };

        var mySettings = $.extend(defaultSettings, options);

        var init = function() {
            var docUrl = "https://spreadsheets.google.com/feeds/list/"+mySettings.gskey+"/od6/public/values";
            $.ajax({
                url: docUrl,
                dataType: "jsonp",
                jsonp: "callback",
                data: {
                    alt: "json"
                }
            }).then(function(data) {
                var students = parseResponse(data);
                pushStudents(students);
                if(mySettings.withForm)
                    prepareForm(students);
            });
            return this;
        }

        var pushStudents = function(students) {
            $.each(students, function(key, value) {
                var $studentBox = $('#student').clone();
                $studentBox.attr('id', 'student-'+key);
                $studentBox.text(value.name);
                var $colorBox = $('<span>');
                $colorBox.css('background-color', value.color);
                $studentBox.prepend($colorBox);
                $self.append($studentBox);
            });
        }

        var parseResponse = function(data) {
            var entries = data.feed.entry;
            var students = [];
            var name = 'gsx$_cn6ca';
            var color = 'gsx$_cokwr';
            $.each(entries, function(key, value) {
                students[key] = {
                    name: value[name].$t,
                    color: value[color].$t
                };
            });
            return students;
        }

        var prepareForm = function(students) {
            var $form = $('<form>');

            var $select = $('<select>').attr('name', 'student-name').addClass('form-input');
            $.each(students, function (key, value) {
                var $option = $('<option>');
                $option.val(key).text(value.name);
                $select.append($option);
            });

            var $color = $('<input type="text">')
                .val('#000000')
                .attr('name', 'student-color')
                .attr('size', '7')
                .attr('maxlength', '7')
                .addClass('form-input');
            $color.ColorPicker({
                onSubmit: function(hsb, hex, rgb, el) {
                    $(el).val('#'+hex);
                    $(el).ColorPickerHide();
                },
                onBeforeShow: function () {
                    $(this).ColorPickerSetColor(this.value);
                }
            });

            var $button = $('<button>').text('Go!');
            $button.click(function(){
                return changeColor();
            });

            $select.appendTo($form);
            $color.appendTo($form);
            $button.appendTo($form);

            $('body').prepend($form);
        }

        var changeColor = function(){
            var currentStudentId = $('select[name="student-name"]').val();
            var currentColor = $('input[name="student-color"]').val();
            var $toBeChanged = $self.find('#student-'+currentStudentId);
            var $colorBox = $toBeChanged.children('span');
            $colorBox.css('background-color', currentColor);
            return false;
        }

        return init();

    };
}(jQuery));